import React from 'react'
import FirstCard from '../Cards/FirstCard';
import gremlin from "./Photos/gremlinDogAustin.jpeg"
import snowman from "./Photos/snowmanDogAustin.jpg"
import athena from "./Photos/athenaDogAustin.jpeg"
import cherry from "./Photos/cherryDogAustin.jpeg"
import {Container, Row, Col, Collapse} from "react-bootstrap";

function Adopt() {

  let dogs = [
    {name: "Gremlin", gender: "Male", location: "Austin, TX", img: gremlin},
    {name: "Snowman", gender: "Male", location: "Austin, TX", img: snowman},
    {name: "Athena", gender: "Female", location: "Austin, TX", img: athena},
    {name: "Cherry", gender: "Female", location: "Austin, TX", img: cherry}
  ]

  function getDog() {

    let obj_one = [];
    for (const dog of dogs) {
      obj_one.push(<Col><FirstCard style={{display: "inline-flex"}} dogInfo={dog}></FirstCard></Col>)
    }

    let obj_two = <Row style={{padding: "1.5rem", marginLeft: "3rem"}}> {obj_one} </Row>;
    return obj_two;

  }

  function getRows() {
    let i;
    const arr = [];

     for (i=0; i < 3; i++) {
       arr.push(getDog());
     }

     return arr;
  }
    return (
        <div className="App bg-dark">
          <Container fluid>

            {getRows()}

          </Container>
        </div>
    )
}

export default Adopt;