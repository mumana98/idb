import React from "react"

function FirstCard(props) {
    return (
        <div class='card-row'>
            <div class="card bg-dark text-white border-white" style={{width: "18rem"}}>
                <img src={props.dogInfo.img} class="card-img-top bg-dark rounded" alt={props.dogInfo.name} />
                <div class="card-body bg-dark mt-2">
                    <p class="card-text"> {props.dogInfo.name} </p>
                    <p class="card-text"> {props.dogInfo.location} </p>
                </div>
            </div>
        </div>
    )
}

export default FirstCard