import React from "react"

function BreedsCard(props) {
    return (
        <div class='card-row'>
            <div class="card d-inline-flex p-2 position-relative bg-dark text-white m-2 border-white" style={{width: "15rem"}}>
                <img src={props.dogInfo.img} class="card-img-top bg-dark rounded" alt={props.dogInfo.breed} />
            </div>
        </div>
    )
}

export default BreedsCard