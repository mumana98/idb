#!/usr/bin/env python3

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
import os

app = Flask(__name__, static_folder="../frontend/build/static", template_folder="../frontend/build")
CORS(app)

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgresql://postgres:1234@34.67.71.158:5432/postgres')
#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:asd123@localhost:5432/adoptiondb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

# ------------
# Models
# ------------

# ------------
# Dog
# ------------
class Dog(db.Model): #creating models
    """ Dog class has seven attrbiutes:
     
    breed
    id
    name
    gender
    color
    img_url
    age

    """
    __tablename__ = 'dog'
    breed = db.Column(db.String(80), nullable = True) 
    id = db.Column(db.Integer, primary_key = True)
    url = db.Column(db.String(200), nullable = False)
    name = db.Column(db.String(120), nullable = False)
    gender = db.Column(db.String(80), nullable = False)
    color = db.Column(db.String(80), nullable = True)
    img_url = db.Column(db.String(200), nullable = False)
    age = db.Column(db.String(80), nullable = False)

    def serialize(self):
        """returns a dictionary"""
        return {
            'id': self.id, 
            'url': self.url,
            'name': self.name,
            'breed': self.breed,
            'gender': self.gender,
            'color': self.color,
            'img': self.img_url,
            'age': self.age
        }

# ------------
# Bird
# ------------
class Bird(db.Model):
    """ Bird class has seven attrbiutes:
     
    breed
    id
    name
    gender
    color
    img_url
    age
    
    """
    __tablename__ = 'bird'
    breed = db.Column(db.String(80), nullable = True) 
    id = db.Column(db.Integer, primary_key = True)
    url = db.Column(db.String(200), nullable = False)
    name = db.Column(db.String(120), nullable = False)
    gender = db.Column(db.String(80), nullable = False)
    color = db.Column(db.String(80), nullable = True)
    img_url = db.Column(db.String(200), nullable = False)
    age = db.Column(db.String(80), nullable = False)

    def serialize(self):
        """returns a dictionary"""
        return {
            'id': self.id, 
            'url': self.url,
            'name': self.name,
            'breed': self.breed,
            'gender': self.gender,
            'color': self.color,
            'img': self.img_url,
            'age': self.age
        }
# ------------
# Cat
# ------------
class Cat(db.Model):
    """ Cat class has seven attrbiutes:
     
    breed
    id
    name
    gender
    color
    img_url
    age
    
    """
    __tablename__ = 'cat'
    breed = db.Column(db.String(80), nullable = True) 
    id = db.Column(db.Integer, primary_key = True)
    url = db.Column(db.String(200), nullable = False)
    name = db.Column(db.String(120), nullable = False)
    gender = db.Column(db.String(80), nullable = False)
    color = db.Column(db.String(80), nullable = True)
    img_url = db.Column(db.String(200), nullable = False)
    age = db.Column(db.String(80), nullable = False)

    def serialize(self):
        """returns a dictionary"""
        return {
            'id': self.id, 
            'url': self.url,
            'name': self.name,
            'breed': self.breed,
            'gender': self.gender,
            'color': self.color,
            'img': self.img_url,
            'age': self.age
        }

#populating
db.drop_all()
db.create_all()
