import requests
import json
from flask import jsonify


'''
base_url = 'https://test1-api.rescuegroups.org/v5' #for rescuegroups api
headers = {'Authorization': '4bANt44Q', 'Content-Type':'application/vnd.api+json'} #for rescuegroups api
'''


#---------------------------------------------------------------------------------------------------------------


base_url = 'https://api.petfinder.com/v2/animals?type=bird&limit=100'
params = 'animals?type=dog&limit=50'
api_key = 'GJAiQIt53w0AA4zdltRVpiSF3hvJOPqtsNCny2MQCN9cVys1E6'
secret = 'R7SSwWfCBNuCznkgx1Pb590bVoQD4r22SylfVC8j'
token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiJHSkFpUUl0NTN3MEFBNHpkbHRSVnBpU0YzaHZKT1BxdHNOQ255Mk1RQ045Y1Z5czFFNiIsImp0aSI6IjU1NzljNzEwNDAxYWNjYWNjZjIzYzI1MmNlZDY2MjE1YWJlNjg3NzJhNDJiYTkyYzMxODc1ZDA3YjY1Zjk3YWE0OWI5NTYyODEwYjYyZGZjIiwiaWF0IjoxNjE3NDAzNzAyLCJuYmYiOjE2MTc0MDM3MDIsImV4cCI6MTYxNzQwNzMwMiwic3ViIjoiIiwic2NvcGVzIjpbXX0.s-OBFMbBX6EIUB-SpYh5Q6XcV8tDkGdaW8KZANZvP3qTTq4kK5uDcLTdjG38mjSMKyzufB_PhWtClzUlWgdv93lcIHsirFq7NfvLMygJoS9psRHbwDahSaX8d0HgD73qGOKoOz6PSqvG8rZ1FjU53ZCcSCU8cscHdX8-EkXX6e-fIG4Z84IcuDzXFl5DMDSyCdluP-up2pawiL6V9WBObifdxdDvgZsSQD9LG12UEGddw_eFXJMf7qvUwMlxFZaEPhVVBz0K4HDinN9w_7QaRtl5r5BxubNv4e71eX7cpXdiILV0STLgTdgoVoPqWPMYwc3WVyq7ALtawCtf8MX44A'
headers = {'Authorization': f'Bearer {token}'}

response = requests.get(base_url, headers=headers)
json_data = json.loads(response.text)

with open('bird_data.json', 'w') as outfile:
    json.dump(json_data, outfile)