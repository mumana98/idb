import os
from flask import Flask, render_template, request, send_from_directory, jsonify
from backend.create_db import app, db, Cat, Bird, Dog, create_dogs, create_birds, create_cats
import json

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
  return render_template("index.html")

@app.route("/api")
def test():
      return "Welcome to the Web API!"

@app.route("/api/dogs")
def test_dog():
      dog_list = db.session.query(Dog).all()
      # print(dog_list)
      return jsonify(Dogs=[dog.serialize() for dog in dog_list])

@app.route("/api/cats")
def test_cat():
      cat_list = db.session.query(Cat).all()
      # print(cat_list)
      return jsonify(Cats=[cat.serialize() for cat in cat_list])

@app.route("/api/birds")
def test_bird():
      bird_list = db.session.query(Bird).all()
      # print(bird_list)
      # for i in bird_list:
      #       print(i.name)
      return jsonify(Birds=[bird.serialize() for bird in bird_list])

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, threaded=True, debug=True)
